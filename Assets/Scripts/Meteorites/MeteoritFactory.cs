using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SpaceMiner.Meteorites
{
  [CreateAssetMenu(fileName = "Meteorit Info", menuName = "ScriptableObjects/Meteorit/Meteorit Info", order = 1)]
  public class MeteoritInfo : ScriptableObject
  {
    [SerializeField] [Min(0.0f)] private float _massMin = 1.0f;
    [SerializeField] [Min(0.0f)] private float _massMax = 5.0f;
    [SerializeField] [Min(0.0f)] private float _velocityMin = 5.0f;
    [SerializeField] [Min(0.0f)] private float _velocityMax = 25.0f;
    [SerializeField] private Mesh _mesh;
    [SerializeField] private Material _materialAlive;
    [SerializeField] private Material _materialDiad;

    public float Mass => Random.Range(_massMin, _massMax);

    public float Velocity => Random.Range(_velocityMin, _velocityMax);
    
    public Mesh Mesh => _mesh;

    public Material MaterialAlive => _materialAlive;
    
    public Material MaterialDead => _materialDiad;

    private void OnValidate()
    {
      _massMin = Mathf.Clamp(_massMin, 0, _massMax);
      _massMax = Mathf.Clamp(_massMax, _massMin, float.MaxValue);
      
      _velocityMin = Mathf.Clamp(_velocityMin, 0, _velocityMax);
      _velocityMax = Mathf.Clamp(_velocityMax, _velocityMin, float.MaxValue);
    }
  }
  
  public class Meteorit : MonoBehaviour
  {
    private Vector3 _direction = Vector3.zero;
    private Vector3 _target = Vector3.zero;
    
    public Vector3 Direction => _direction + transform.position;

    public Vector3 Target
    {
      get => _target;
      set
      {
        _target = value;
        _direction = (_target - transform.position).normalized;
      }
    }
    
    public float Velocity { get; set; }
    public Rigidbody Rigidbody { get; set; }

    private void FixedUpdate()
    {
      Rigidbody.AddForce(Direction * Velocity, ForceMode.Force);
    }
  }
  
  public abstract class MeteoritFactory : ScriptableObject
  {
    [SerializeField] private MeteoritInfo _info;

    public MeteoritInfo Info => _info;
    
    public abstract Meteorit Create();
  }

  [CreateAssetMenu(fileName = "Simple Meteorit Factory", menuName = "ScriptableObjects/Meteorit/Factory/Simple Meteorit", order = 1)]
  public class SimpleMeteoritFactory : MeteoritFactory
  {
    public override Meteorit Create()
    {
      var obj = new GameObject("Meteorit Simple");
      
      var meshFilter = obj.AddComponent<MeshFilter>();
      meshFilter.mesh = Info.Mesh;
      
      var meshRenderer = obj.AddComponent<MeshRenderer>();
      meshRenderer.sharedMaterial = Info.MaterialAlive;
      
      var rigidbody = obj.AddComponent<Rigidbody>();
      rigidbody.mass = Info.Mass;

      var meteorit = obj.AddComponent<Meteorit>();
      meteorit.Rigidbody = rigidbody;
      meteorit.Velocity = Info.Velocity;
      
      return meteorit;
    }
  }
}
