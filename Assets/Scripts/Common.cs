﻿using UnityEngine;

namespace SpaceMiner
{
  public delegate void ShootingDelegate(Vector3[] targets);
  public delegate void TickDelegate(float dt);
}