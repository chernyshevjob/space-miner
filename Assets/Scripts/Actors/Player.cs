﻿using System;
using SpaceMiner.Architecture;
using UnityEngine;

namespace SpaceMiner.Actors
{
  public class IdleState : IState<Player>
  {
    public string Name => "Player Idle";
    public IState<Player> State { get; set; }
    public void Initialize(Player context)
    {
      
    }

    public IState<Player> Execute(Player context)
    {
      return State;
    }

    public void Finalize(Player context)
    {
      
    }
  }

  public class Player : StateMachineBehaviour<Player>
  {
    public event ShootingDelegate Shooting;
    public event Action Nothing;

    public Vector3 Positon => transform.position;
    
    public void CallShooting(Vector3[] targets)
    {
      Shooting?.Invoke(targets);
    }

    public void CallNothing()
    {
      Nothing?.Invoke();
    }
    
    public Player(Player context, IState<Player> state) : base(context, state)
    {
    }

    protected override (Player, IState<Player>) Initialization()
    {
      return (this, new IdleState());
    }

    protected override void Finalization()
    {
      
    }
  }
}