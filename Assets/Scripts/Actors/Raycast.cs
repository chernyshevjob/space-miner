using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceMiner.Actors
{
  public class Raycast : MonoBehaviour
  {
    public event ShootingDelegate Shooting;
    public event Action Nothing;
    
    private UnityEngine.Camera _camera = null;

    private void Awake()
    {
      _camera = UnityEngine.Camera.main;
    }

    private void Update()
    {
    #if UNITY_EDITOR
      RaycastMouse();
    #else
      RaycastTouches();
    #endif
    }

    private void RaycastMouse()
    {
      if (!Input.GetKey(KeyCode.Mouse0))
      {
        Nothing?.Invoke();
        return;
      }

      var pointHits = new Vector3[] {PointHit(Input.mousePosition)};

      Shooting?.Invoke(pointHits);
    }

    private void RaycastTouches()
    {
      if (!(Input.touchCount > 0))
      {
        Nothing?.Invoke();
        return;
      }
      
      var pointHits = new Vector3[Input.touchCount];

      for (int i = 0; i < Input.touchCount; i++)
      {
        pointHits[i] = PointHit(Input.touches[i].position);
      }

      Shooting?.Invoke(pointHits);
    }
    
    private Vector3 PointHit(Vector3 point)
    {
      Ray ray = _camera.ScreenPointToRay(point);
      Vector3 target = ray.origin;
      target.y = 0;
      return target;
      /*
      return (Physics.Raycast(ray, out RaycastHit hit) && hit.transform != null && hit.transform.CompareTag("Point"), 
        hit.transform != null ? hit.transform : null);
        */
    }
  }
}
