using System.Collections;
using System.Collections.Generic;
using SpaceMiner.Actors;
using UnityEngine;

public class Director : MonoBehaviour
{
    [SerializeField] private Raycast _raycast;
    [SerializeField] private Player _player;

    private void Start()
    {
        _raycast.Shooting += _player.CallShooting;
        _raycast.Nothing += _player.CallNothing;
    }
}
