﻿using UnityEngine;

namespace SpaceMiner.Weapons
{
  public class Weapon
  {
    private float _t = 0.0f;
    private float _time = 0.0f;

    private Vector3 _pointSpawn;
    private BulletInfo _bulletInfo;
    public Weapon(BulletInfo bulletInfo, float time, Vector3 pointSpawn)
    {
      _time = time;
      _pointSpawn = pointSpawn;
      _bulletInfo = bulletInfo;
    }
    
    public void Shoot(Vector3 target)
    {
      if (_t < _time) return;

      GameObject obj = MonoBehaviour.Instantiate(_bulletInfo.Bullet, _pointSpawn, Quaternion.identity);
      Bullet bullet = obj.AddComponent<Bullet>();
      bullet.Direction = (target - _pointSpawn).normalized;  
      
      _bulletInfo.Setup(bullet);

      _t = 0.0f;
    }

    public void Update(float dt)
    {
      _t += dt;
    }
  }
}