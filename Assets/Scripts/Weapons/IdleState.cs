﻿using SpaceMiner.Architecture;
using UnityEngine;

namespace SpaceMiner.Weapons
{
  public class IdleState : IState<Turret>
  {
    public string Name => "Turret Idle State";
    public IState<Turret> State { get; set; }
    public void Initialize(Turret context)
    {
      _context = context;
      _info = context.Info;
      context.Shooting += ContextOnShooting;
    }

    private Turret _context = null;
    private TurretInfo _info = null;
    
    private void ContextOnShooting(Vector3[] targets)
    {
      Debug.Log("Call On Shoot Method Event");
      State = new FireState();
    }

    public IState<Turret> Execute(Turret context)
    {
      return State;
    }

    public void Finalize(Turret context)
    {
      context.Shooting -= ContextOnShooting;
    }
  }
}