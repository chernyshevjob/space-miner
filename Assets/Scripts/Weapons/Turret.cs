﻿using System;
using System.Collections.Generic;
using SpaceMiner.Actors;
using SpaceMiner.Architecture;
using SpaceMiner.Weapons;
using UnityEngine;

namespace SpaceMiner.Weapons
{
  /*
  public interface IManager
  {
    void Initialization();
    void Update(float dt);
    void Finalization();
  }
  
  public class BulletManager : List<Bullet>
  {
    public BulletManager() : base(new List<Bullet>())
    {
      
    }
  }

  public struct BulletComponent
  {
    public Vector3 Direction;
    public float Speed;
  }
  */
  public class Bullet : MonoBehaviour
  {
    public TrailRenderer TrailRenderer { get; private set; }
    public Vector3 Direction { get; set; } = Vector3.zero;
    public float Speed { get; set; } = 0.0f;

    private void Awake()
    {
      TrailRenderer = gameObject.AddComponent<TrailRenderer>();
      Destroy(gameObject, 2.0f);
    }
    
    private void Update ()
    {
      transform.Translate(Direction * Speed * Time.deltaTime);
    }
  }
  
  public class Turret : StateMachineBehaviour<Turret>
  {
    public event ShootingDelegate Shooting;
    public event Action Nothing;
    
    [SerializeField] private TurretInfo _info; 
    [HideInInspector] public Player _player = null;

    private Weapon _weapon = null;
    
    public TurretInfo Info => _info;

    public Turret(Turret context, IState<Turret> state) : base(context, state)
    {
    }
    
    public Quaternion LookAtLerp(Transform my, Vector3 target)
    {
      Quaternion lookRotation = Quaternion.LookRotation(target - my.position);
      return Quaternion.Lerp(my.rotation, lookRotation, Time.deltaTime * _info.SpeedTurn);
    }

    public bool IsCanShoot(Transform my, Vector3 target)
    {
      Vector3 dirForTarget = (target - my.position).normalized;
      Vector3 dirForForward = my.forward;

      float angle = Vector3.Angle(dirForForward, dirForTarget);

      return angle < 10.0f;
    }
    
    public void Shoot(Vector3 target)
    {
      _weapon.Shoot(target);
    }

    private void CallShooting(Vector3[] targets)
    {
      Shooting?.Invoke(targets);
    }

    private void CallNothing()
    {
      Nothing?.Invoke();
    }
    
    protected override (Turret, IState<Turret>) Initialization()
    {
      BulletInfo bulletInfo = _info.BulletInfo;
      _weapon = new Weapon(bulletInfo, _info.TimeFire, transform.position);
      Tick += _weapon.Update;
      
      _player = GetComponentInParent<Player>();
  
      if (_player == null)
      {
        Debug.LogError($"Not found '{typeof(Player)}' component on parent");
        return (this, null);
      }

      _player.Shooting += CallShooting;
      _player.Nothing += CallNothing;

      transform.parent = null;
      
      return (this, new IdleState());
    }

    protected override void Finalization()
    {
      _player.Shooting -= CallShooting;
      _player.Nothing -= CallNothing;
    }
  }
}