﻿using System;
using SpaceMiner.Architecture;
using UnityEngine;

namespace SpaceMiner.Weapons
{
  public class FireState : IState<Turret>
  {
    public string Name => "Turret Fire State";
    public IState<Turret> State { get; set; }

    private Turret _context = null;

    private Transform _my;
    private Func<Transform, Vector3, bool> IsCanShoot;
    private Action<Vector3> Shoot;
    
    public void Initialize(Turret context)
    {
      _context = context;
      _my = _context.transform;
      IsCanShoot = context.IsCanShoot;
      Shoot = context.Shoot;
      
      context.Shooting += ContextOnShooting;
      context.Nothing += ContextOnNothing;
    }

    private void ContextOnNothing()
    {
      State = new IdleState();
    }

    public IState<Turret> Execute(Turret context)
    {
      return State;
    }

    public void Finalize(Turret context)
    {
      context.Shooting -= ContextOnShooting;
      context.Nothing -= ContextOnNothing;
    }
    
    private void ContextOnShooting(Vector3[] targets)
    {
      Transform t = _context.transform;
      Vector3 target = ChooseTarget(targets);
      t.rotation = _context.LookAtLerp(t, target);

      if (!IsCanShoot(_my, target)) return;
      
      Shoot(target);
    }
    
    private Vector3 ChooseTarget(Vector3[] targets)
    {
      Vector3 selfPos = _context.transform.position;
      
      if (targets.Length < 2)
        return targets[0];
      
      Array.Sort(targets, (vL, vR) =>
      {
        float dL = Vector3.Distance(selfPos, vL);
        float dR = Vector3.Distance(selfPos, vR);
        return dL < dR ? -1 : 1;
      });

      return targets[0];
    }
  }
}