﻿using UnityEngine;

namespace SpaceMiner.Weapons
{
  [CreateAssetMenu(fileName = "Turret Info", menuName = "ScriptableObjects/Turret Info", order = 1)]
  public class TurretInfo : ScriptableObject
  {
    [SerializeField] [Min(0.0f)] [Tooltip("Скорость поворота в направлении цели")] private float _speedTurn = 1.0f;
    [SerializeField] [Min(1)] [Tooltip("Скорострельность. Кол-во выстрелов в минуту")] private int _rateOfFire = 1;
    [SerializeField] [Tooltip("Информация о снаряде")] private BulletInfo _bulletInfo;
    
    /// <summary>
    /// Скорость поворота в направлении цели
    /// </summary>
    public float SpeedTurn => _speedTurn;
    /// <summary>
    /// Кол-во выстрелов в минуту
    /// </summary>
    public int RateOfFire => _rateOfFire;
    /// <summary>
    /// Задержка между выстрелами
    /// </summary>
    public float TimeFire => 1.0f / _rateOfFire; //! ВОЗМОЖНО НАОБОРОТ
    /// <summary>
    /// Информация о снаряде
    /// </summary>
    public BulletInfo BulletInfo => _bulletInfo;
  }
}