﻿using UnityEngine;

namespace SpaceMiner.Weapons
{
  [CreateAssetMenu(fileName = "Bullet Info", menuName = "ScriptableObjects/Bullet Info", order = 1)]
  public class BulletInfo : ScriptableObject
  {
    [SerializeField] [Min(0.0f)] [Tooltip("Скорость полета снаряда")] private float _velocity = 1.0f;
    [SerializeField] [Tooltip("Объект снаряда")] private GameObject _bullet;
    [SerializeField] private AnimationCurve _widthCurve;
    [SerializeField] private float _time = 0.5f;
    [SerializeField] private float _minVerterxDistance = 0.1f;
    [SerializeField] private Gradient _colorGradient;
    [SerializeField] private Material _material;
    [SerializeField] private int _cornerVertices;
    [SerializeField] private int _endCapVertices;
    /// <summary>
    /// Скорость полета снаряда
    /// </summary>
    public float Velocity => _velocity;
    /// <summary>
    /// Объект снаряда
    /// </summary>
    public GameObject Bullet => _bullet;


    public void Setup(Bullet bullet)
    {
      SetupBullet(bullet);
      SetupTrail(bullet.TrailRenderer);
    }

    private void SetupBullet(Bullet bullet)
    {
      bullet.Speed = _velocity;
    }
    
    private void SetupTrail(TrailRenderer trailRenderer)
    {
      trailRenderer.widthCurve = _widthCurve;
      trailRenderer.time = _time;
      trailRenderer.minVertexDistance = _minVerterxDistance;
      trailRenderer.colorGradient = _colorGradient;
      trailRenderer.sharedMaterial = _material;
      trailRenderer.numCornerVertices = _cornerVertices;
      trailRenderer.numCapVertices = _endCapVertices;
    }
  }
}