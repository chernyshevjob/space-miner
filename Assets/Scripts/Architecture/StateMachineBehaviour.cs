﻿using System;
using UnityEngine;

namespace SpaceMiner.Architecture
{
  public abstract class StateMachineBehaviour<T> : MonoBehaviour
  {
    public event TickDelegate Tick;
    
    private T _context;
    private IState<T> _currenState = null;
    
    protected StateMachineBehaviour(T context, IState<T> state)
    {
      //Init(context, state);
    }

    private void Init (T context, IState<T> state)
    {
      _context = context;
      
      if (state == null) return;
      
      _currenState = state;
      _currenState.State = state;
      _currenState.Initialize(context);
    }

    private void Awake()
    {
      (T context, IState<T> state) = Initialization();
      Init(context, state);
    }

    private void OnDestroy()
    {
      Finalization();
    }

    protected abstract (T,IState<T>) Initialization();
    protected abstract void Finalization();

    private void Update()
    {
      UpdateState();
      
      Tick?.Invoke(Time.deltaTime);
    }

    private void UpdateState()
    {
      if (_currenState == null) return;
        
      var state = _currenState.Execute(_context);
        
      if (state == _currenState) return;
      _currenState.Finalize(_context);
      _currenState = state;
      _currenState.State = state;
        
#if DEBUG
      Debug.Log($"Change {typeof(T)} of State to: {state.Name}");
#endif
      _currenState.Initialize(_context);
    }
  }
}