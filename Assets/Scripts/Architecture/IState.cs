﻿namespace SpaceMiner.Architecture
{
  public interface IState<T>
  {
    string Name { get; }

    IState<T> State { get; set; }

    void Initialize(T context);

    IState<T> Execute(T context);

    void Finalize(T context);
  }
}