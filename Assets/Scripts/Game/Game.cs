﻿using SpaceMiner.Architecture;
using UnityEngine;

namespace SpaceMiner.Game
{
  public class Game : StateMachineBehaviour<Game>
  {
    public Game(Game context, IState<Game> state) : base(context, state)
    {
    }

    protected override (Game, IState<Game>) Initialization()
    {
      return (this, null);
    }

    protected override void Finalization()
    {
      
    }
  }
}